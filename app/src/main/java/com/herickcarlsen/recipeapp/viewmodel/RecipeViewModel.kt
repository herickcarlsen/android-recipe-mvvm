package com.herickcarlsen.recipeapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.herickcarlsen.recipeapp.entities.CategoryItems
import com.herickcarlsen.recipeapp.entities.FavMeals
import com.herickcarlsen.recipeapp.entities.MealsItems
import com.herickcarlsen.recipeapp.repository.RecipeRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class RecipeViewModel @Inject constructor(private val repository: RecipeRepository): ViewModel() {

    fun getAllCategory(): LiveData<List<CategoryItems>> {
        return repository.getAllCategory()
    }

    fun insertCategory(categoryItems: CategoryItems?) {
        CoroutineScope(Dispatchers.IO).launch {
            repository.insertCategory(categoryItems)
        }
    }

    fun insertMeal(mealsItems: MealsItems?) {
        CoroutineScope(Dispatchers.IO).launch {
            repository.insertMeal(mealsItems)
        }
    }

    fun clearDb() {
        CoroutineScope(Dispatchers.IO).launch {
            repository.clearDb()
        }
    }

    fun getSpecificMealList(categoryName:String): LiveData<List<MealsItems>> {
        return repository.getSpecificMealList(categoryName)
    }

    fun getMealFilter(strMeal:String): LiveData<List<MealsItems>> {
        return repository.getMealFilter(strMeal)
    }

    fun favMeal(favMeals: FavMeals){
        CoroutineScope(Dispatchers.IO).launch {
            repository.favMeal(favMeals)
        }
    }

    fun deleteFavMeal(idMeal: String){
        CoroutineScope(Dispatchers.IO).launch {
            repository.deleteFavMeal(idMeal)
        }
    }



}