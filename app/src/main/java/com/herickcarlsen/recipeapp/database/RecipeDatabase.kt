package com.herickcarlsen.recipeapp.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.herickcarlsen.recipeapp.dao.RecipeDao
import com.herickcarlsen.recipeapp.entities.*


@Database(entities = [CategoryItems::class,MealsItems::class,FavMeals::class],version = 1,exportSchema = false)
abstract class RecipeDatabase: RoomDatabase() {
    abstract fun recipeDao():RecipeDao
}