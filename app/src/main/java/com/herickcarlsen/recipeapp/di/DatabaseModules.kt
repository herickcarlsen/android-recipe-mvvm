package com.herickcarlsen.recipeapp.di

import android.content.Context
import androidx.room.Room
import com.herickcarlsen.recipeapp.dao.RecipeDao
import com.herickcarlsen.recipeapp.database.RecipeDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class DatabaseModules {

    @Provides
    fun provideRecipeDao(db: RecipeDatabase): RecipeDao {

        return db.recipeDao()

    }

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): RecipeDatabase {
        return Room.databaseBuilder(
            context,
            RecipeDatabase::class.java,
            "recipe.db"
        ).build()

    }

}