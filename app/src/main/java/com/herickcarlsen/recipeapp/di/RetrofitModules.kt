package com.herickcarlsen.recipeapp.di

import com.herickcarlsen.recipeapp.interfaces.GetDataService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class RetrofitModules {

    val BASE_URL = "https://www.themealdb.com/api/json/v1/1/"

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit{
    return Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    }

    @Provides
    fun provideDataService(retrofit: Retrofit): GetDataService {
        return retrofit.create(GetDataService::class.java)
    }

}