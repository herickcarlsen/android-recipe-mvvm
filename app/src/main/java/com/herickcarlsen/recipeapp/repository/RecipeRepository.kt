package com.herickcarlsen.recipeapp.repository

import androidx.lifecycle.LiveData
import com.herickcarlsen.recipeapp.dao.RecipeDao
import com.herickcarlsen.recipeapp.entities.CategoryItems
import com.herickcarlsen.recipeapp.entities.FavMeals
import com.herickcarlsen.recipeapp.entities.MealsItems
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class RecipeRepository @Inject constructor(
    private val dao: RecipeDao
) {

    fun getAllCategory(): LiveData<List<CategoryItems>> {
        return dao.getAllCategory()
    }

    fun insertCategory(categoryItems: CategoryItems?) {
        CoroutineScope(Dispatchers.IO).launch {
            dao.insertCategory(categoryItems)
        }
    }

    fun insertMeal(mealsItems: MealsItems?) {
        CoroutineScope(Dispatchers.IO).launch {
            dao.insertMeal(mealsItems)
        }
    }

    fun clearDb() {
        CoroutineScope(Dispatchers.IO).launch {
            dao.clearDb()
        }
    }

    fun getSpecificMealList(categoryName:String): LiveData<List<MealsItems>> {
        return dao.getSpecificMealList(categoryName)
    }

    fun getMealFilter(strMeal:String): LiveData<List<MealsItems>> {
        return dao.getMealFilter(strMeal)
    }

    fun favMeal(favMeals: FavMeals){
        CoroutineScope(Dispatchers.IO).launch {
            dao.favMeal(favMeals)
        }
    }

    fun deleteFavMeal(idMeal: String){
        CoroutineScope(Dispatchers.IO).launch {
            dao.deleteFavMeal(idMeal)
        }
    }

}