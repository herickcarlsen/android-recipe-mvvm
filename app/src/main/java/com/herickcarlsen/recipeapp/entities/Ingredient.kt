package com.herickcarlsen.recipeapp.entities

data class Ingredient(
    var ingredient: String,
    var measure:String
)
