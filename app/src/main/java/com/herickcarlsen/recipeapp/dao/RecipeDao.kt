package com.herickcarlsen.recipeapp.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.herickcarlsen.recipeapp.entities.CategoryItems
import com.herickcarlsen.recipeapp.entities.FavMeals
import com.herickcarlsen.recipeapp.entities.MealsItems

@Dao
interface RecipeDao {

    @Query("SELECT * FROM categoryitems ORDER BY id DESC")
    fun getAllCategory() : LiveData<List<CategoryItems>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategory(categoryItems: CategoryItems?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMeal(mealsItems: MealsItems?)

    @Query("DELETE FROM categoryitems")
    fun clearDb()

    @Query("SELECT * FROM MealItems WHERE categoryName = :categoryName ORDER BY id DESC")
    fun getSpecificMealList(categoryName:String) : LiveData<List<MealsItems>>

    @Query("SELECT * FROM MealItems WHERE strMeal LIKE '%' || :strMeal || '%'")
    fun getMealFilter(strMeal:String) : LiveData<List<MealsItems>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun favMeal(favMeals: FavMeals)

    @Query("DELETE FROM FavMeals WHERE idMeal = :idMeal")
    fun deleteFavMeal(idMeal: String)

}