package com.herickcarlsen.recipeapp.ui.activity

import android.os.Bundle
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import com.herickcarlsen.recipeapp.R
import com.herickcarlsen.recipeapp.ui.fragment.*
import dagger.hilt.android.AndroidEntryPoint


private const val SPLASH_TAG = "splash_tag"
private const val HOME_TAG = "home_tag"
private const val DETAIL_TAG = "detail_tag"


@AndroidEntryPoint
class MainActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        whenAttachFragment()
        openMain()
    }



    private fun configSingleActivityBackPressedButton(fragment: String){
        onBackPressedDispatcher.addCallback(this) {
               when(fragment){
                   SPLASH_FRAGMENT -> finish()
                   HOME_FRAGMENT -> finish()
                   DETAIL_FRAGMENT -> openHome(DETAIL_FRAGMENT)
               }
        }
    }

    private fun whenAttachFragment(){
        supportFragmentManager.addFragmentOnAttachListener { _, fragment ->
            if(fragment is SplashFragment){
                configSingleActivityBackPressedButton(SPLASH_FRAGMENT)

                fragment.openHomeFragment = {
                    openHome(it)
                }
            }
            if(fragment is HomeFragment){
                configSingleActivityBackPressedButton(HOME_FRAGMENT)

                fragment.openDetailFragment = {
                    openDetail(it)
                }
            }

            if(fragment is DetailFragment){
                configSingleActivityBackPressedButton(DETAIL_FRAGMENT)
                fragment.openHomeFragment = {
                    openHome(it)
                }
            }
        }
    }



    private fun openMain(){
        val beginTransaction = supportFragmentManager.beginTransaction()
        val fragment = SplashFragment()
        beginTransaction.add(R.id.activity_main_container,fragment,SPLASH_TAG)
        beginTransaction.commit()
    }

    private fun openHome(currentFragment:String){


        var findFragmentByTag:Fragment? = null

        when(currentFragment){
            SPLASH_FRAGMENT -> findFragmentByTag = supportFragmentManager.findFragmentByTag(SPLASH_TAG)
            DETAIL_FRAGMENT -> findFragmentByTag = supportFragmentManager.findFragmentByTag(DETAIL_TAG)
        }
        val beginTransaction = supportFragmentManager.beginTransaction()
        val fragment = HomeFragment()

        findFragmentByTag?.let{removeFragment ->
            beginTransaction.remove(removeFragment)
        }

        beginTransaction.add(R.id.activity_main_container,fragment, HOME_TAG)
        beginTransaction.commit()
    }

    private fun openDetail(id:String){
        val findFragmentByTag = supportFragmentManager.findFragmentByTag(HOME_TAG)
        val beginTransaction = supportFragmentManager.beginTransaction()
        val fragment = DetailFragment()
        val data = Bundle()
        data.putString("id",id)
        fragment.arguments = data

        findFragmentByTag?.let{removeFragment ->
            beginTransaction.remove(removeFragment)
        }

        beginTransaction.add(R.id.activity_main_container,fragment, DETAIL_TAG)
        beginTransaction.commit()
    }


}