package com.herickcarlsen.recipeapp.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.herickcarlsen.recipeapp.RecipeViewModel
import com.herickcarlsen.recipeapp.interfaces.GetDataService
import com.herickcarlsen.recipeapp.repository.RecipeRepository
import com.herickcarlsen.recipeapp.viewmodel.factory.RecipeViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

@AndroidEntryPoint
open class BaseActivity : AppCompatActivity(),CoroutineScope {

    @Inject
    lateinit var repository:RecipeRepository

    @Inject
    lateinit var service:GetDataService

    val viewModel by lazy{
        val factory = RecipeViewModelFactory(repository)
        val provider = ViewModelProvider(this,factory)
        provider.get(RecipeViewModel::class.java)
    }


    private lateinit var job: Job
    override val coroutineContext:CoroutineContext
    get() = job +Dispatchers.Main


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        job = Job()
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

}