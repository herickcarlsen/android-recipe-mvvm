package com.herickcarlsen.recipeapp.ui.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.herickcarlsen.recipeapp.R
import com.herickcarlsen.recipeapp.RecipeViewModel
import com.herickcarlsen.recipeapp.adapter.IngredientAdapter
import com.herickcarlsen.recipeapp.entities.CategoryItems
import com.herickcarlsen.recipeapp.entities.FavMeals
import com.herickcarlsen.recipeapp.entities.Ingredient
import com.herickcarlsen.recipeapp.entities.MealResponse
import com.herickcarlsen.recipeapp.interfaces.GetDataService
import com.herickcarlsen.recipeapp.ui.activity.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.IllegalArgumentException


const val DETAIL_FRAGMENT = "detail_fragment"


@AndroidEntryPoint
class DetailFragment : Fragment() {

    var youtubeLink = ""
    private lateinit var viewModel : RecipeViewModel
    private lateinit var service : GetDataService


    private var arrIngredient = ArrayList<Ingredient>()
    private var ingredientAdapter = IngredientAdapter()

    var openHomeFragment: (currentFragment:String) -> Unit = {}


    private val id: String by lazy{
        arguments?.getString("id") ?: throw IllegalArgumentException("Invalid Id")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = (activity as BaseActivity).viewModel
        service = (activity as BaseActivity).service

        getSpecificItem(id)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.detail,container,false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        imgToolbarBtnBack.setOnClickListener {
            openHomeFragment(DETAIL_FRAGMENT)
        }

        imgToolbarBtnFav.setOnClickListener {

            Toast.makeText(context, "Fav button not working yet", Toast.LENGTH_LONG).show()
            viewModel.favMeal(FavMeals(id))
        }

        btnRecipeLink.setOnClickListener {
            val uri = Uri.parse(youtubeLink)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }

        super.onViewCreated(view, savedInstanceState)
    }


    private fun getSpecificItem(id:String) {
        val call = service.getSpecificItem(id)
        call.enqueue(object : Callback<MealResponse> {
            override fun onFailure(call: Call<MealResponse>, t: Throwable) {

                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<MealResponse>,
                response: Response<MealResponse>
            ) {

                Glide.with(this@DetailFragment).load(response.body()!!.mealsEntity[0].strmealthumb).into(imgItem)


                if(response.body()!!.mealsEntity[0].stringredient1 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient1,response.body()!!.mealsEntity[0].strmeasure1))
                if(response.body()!!.mealsEntity[0].stringredient2 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient2,response.body()!!.mealsEntity[0].strmeasure2))
                if(response.body()!!.mealsEntity[0].stringredient3 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient3,response.body()!!.mealsEntity[0].strmeasure3))
                if(response.body()!!.mealsEntity[0].stringredient4 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient4,response.body()!!.mealsEntity[0].strmeasure4))
                if(response.body()!!.mealsEntity[0].stringredient5 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient5,response.body()!!.mealsEntity[0].strmeasure5))
                if(response.body()!!.mealsEntity[0].stringredient6 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient6,response.body()!!.mealsEntity[0].strmeasure6))
                if(response.body()!!.mealsEntity[0].stringredient7 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient7,response.body()!!.mealsEntity[0].strmeasure7))
                if(response.body()!!.mealsEntity[0].stringredient8 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient8,response.body()!!.mealsEntity[0].strmeasure8))
                if(response.body()!!.mealsEntity[0].stringredient9 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient9,response.body()!!.mealsEntity[0].strmeasure9))
                if(response.body()!!.mealsEntity[0].stringredient10 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient10,response.body()!!.mealsEntity[0].strmeasure10))
                if(response.body()!!.mealsEntity[0].stringredient11 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient11,response.body()!!.mealsEntity[0].strmeasure11))
                if(response.body()!!.mealsEntity[0].stringredient12 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient12,response.body()!!.mealsEntity[0].strmeasure12))
                if(response.body()!!.mealsEntity[0].stringredient13 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient13,response.body()!!.mealsEntity[0].strmeasure13))
                if(response.body()!!.mealsEntity[0].stringredient14 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient14,response.body()!!.mealsEntity[0].strmeasure14))
                if(response.body()!!.mealsEntity[0].stringredient15 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient15,response.body()!!.mealsEntity[0].strmeasure15))
                if(response.body()!!.mealsEntity[0].stringredient16 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient16,response.body()!!.mealsEntity[0].strmeasure16))
                if(response.body()!!.mealsEntity[0].stringredient17 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient17,response.body()!!.mealsEntity[0].strmeasure17))
                if(response.body()!!.mealsEntity[0].stringredient18 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient18,response.body()!!.mealsEntity[0].strmeasure18))
                if(response.body()!!.mealsEntity[0].stringredient19 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient19,response.body()!!.mealsEntity[0].strmeasure19))
                if(response.body()!!.mealsEntity[0].stringredient20 != "")
                    arrIngredient.add(Ingredient(response.body()!!.mealsEntity[0].stringredient20,response.body()!!.mealsEntity[0].strmeasure20))


                ingredientAdapter.setData(arrIngredient)
                rv_ingredient.layoutManager = LinearLayoutManager(context,
                    LinearLayoutManager.HORIZONTAL,false)
                rv_ingredient.adapter = ingredientAdapter

                tvInstructions.text = response.body()!!.mealsEntity[0].strinstructions

                youtubeLink = response.body()!!.mealsEntity[0].strsource
            }

        })
    }

}