package com.herickcarlsen.recipeapp.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.herickcarlsen.recipeapp.*
import com.herickcarlsen.recipeapp.adapter.MainCategoryAdapter
import com.herickcarlsen.recipeapp.adapter.SubCategoryAdapter
import com.herickcarlsen.recipeapp.entities.CategoryItems
import com.herickcarlsen.recipeapp.entities.MealsItems
import com.herickcarlsen.recipeapp.ui.activity.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.home.*


const val HOME_FRAGMENT = "home_fragment"


@AndroidEntryPoint
class HomeFragment : Fragment() {

    private var arrMainCategory = ArrayList<CategoryItems>()
    private var arrSubCategory = ArrayList<MealsItems>()

    private var mainCategoryAdapter = MainCategoryAdapter()
    private var subCategoryAdapter = SubCategoryAdapter()

    private lateinit var viewModel : RecipeViewModel

    var openDetailFragment: (id:String) -> Unit = {}


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = (activity as BaseActivity).viewModel

        getDataFromDb()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.home,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainCategoryAdapter.setClickListener(onCLicked)
        subCategoryAdapter.setClickListener(onCLickedSubItem)
        searchViewConfig()
    }

    private fun searchViewConfig() {

        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                Log.d(tag, "Submit = $query")
                getMealFilter(query)
                return false
            }
            override fun onQueryTextChange(query: String): Boolean {
                Log.d(tag, "Change = $query")
                if(query =="") getDataFromDb()
                return false
            }
        })
    }


    private val onCLicked  = object : MainCategoryAdapter.OnItemClickListener{
        override fun onClicked(categoryName: String) {
            getMealDataFromDb(categoryName)
        }
    }

    private val onCLickedSubItem  = object : SubCategoryAdapter.OnItemClickListener{
        override fun onClicked(id: String) {
            openDetailFragment(id)
        }
    }

    private fun getDataFromDb(){
        val cat = viewModel.getAllCategory()
        cat.observe(this@HomeFragment, Observer {
            arrMainCategory = it as ArrayList<CategoryItems>
            arrMainCategory.reverse()
            getMealDataFromDb(arrMainCategory[0].strcategory)
            mainCategoryAdapter.setData(arrMainCategory)
            rv_main_category.layoutManager = LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL,false)
            rv_main_category.adapter = mainCategoryAdapter

        })
    }



    @SuppressLint("SetTextI18n")
    private fun getMealFilter(mealName:String){

        val cat = viewModel.getMealFilter(mealName)

        cat.observe(this@HomeFragment, Observer {
            arrSubCategory = it as ArrayList<MealsItems>
            subCategoryAdapter.setData(arrSubCategory)
            rv_sub_category.layoutManager = LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL,false)
            rv_sub_category.adapter = subCategoryAdapter
        })

    }

    @SuppressLint("SetTextI18n")
    private fun getMealDataFromDb(categoryName:String){
//        tvCategory.text = "$categoryName Category"
        val cat = viewModel.getSpecificMealList(categoryName)
        cat.observe(this@HomeFragment, Observer {
            arrSubCategory = it as ArrayList<MealsItems>
            subCategoryAdapter.setData(arrSubCategory)
            rv_sub_category.layoutManager = LinearLayoutManager(context,
                LinearLayoutManager.HORIZONTAL,false)
            rv_sub_category.adapter = subCategoryAdapter
        })
    }


}