package com.herickcarlsen.recipeapp.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.herickcarlsen.recipeapp.R
import com.herickcarlsen.recipeapp.RecipeViewModel
import com.herickcarlsen.recipeapp.entities.Category
import com.herickcarlsen.recipeapp.entities.Meal
import com.herickcarlsen.recipeapp.entities.MealsItems
import com.herickcarlsen.recipeapp.interfaces.GetDataService
import com.herickcarlsen.recipeapp.ui.activity.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.splash.*
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


const val SPLASH_FRAGMENT = "splash_fragment"


@AndroidEntryPoint
class SplashFragment : Fragment(), EasyPermissions.RationaleCallbacks,
    EasyPermissions.PermissionCallbacks {

    private lateinit var viewModel : RecipeViewModel
    private lateinit var service : GetDataService
    var openHomeFragment: (currentFragment:String) -> Unit = {}

    private var READ_STORAGE_PERM = 123

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = (activity as BaseActivity).viewModel
        service = (activity as BaseActivity).service
        readStorageTask()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.splash,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnGetStarted.setOnClickListener {
            openHomeFragment(SPLASH_FRAGMENT)
        }

    }


    private fun getCategories() {
        val call = service.getCategoryList()
        call.enqueue(object : Callback<Category> {
            override fun onFailure(call: Call<Category>, t: Throwable) {

                Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<Category>,
                response: Response<Category>
            ) {

                for (arr in response.body()!!.categorieitems!!) {
                    getMeal(arr.strcategory)
                }
                insertDataIntoRoomDb(response.body())
            }

        })
    }


    fun getMeal(categoryName: String) {
        val call = service.getMealList(categoryName)
        call.enqueue(object : Callback<Meal> {
            override fun onFailure(call: Call<Meal>, t: Throwable) {

                loader.visibility = View.INVISIBLE
                Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<Meal>,
                response: Response<Meal>
            ) {

                insertMealDataIntoRoomDb(categoryName, response.body())
            }

        })
    }

    fun insertDataIntoRoomDb(category: Category?) {

        for (arr in category!!.categorieitems!!) {
            viewModel.insertCategory(arr)
        }

    }

    fun insertMealDataIntoRoomDb(categoryName: String, meal: Meal?) {

        for (arr in meal!!.mealsItem!!) {
            var mealItemModel = MealsItems(
                arr.id,
                arr.idMeal,
                categoryName,
                arr.strMeal,
                arr.strMealThumb
            )

            viewModel.insertMeal(mealItemModel)

            Log.d("mealData", arr.toString())
        }

        btnGetStarted.visibility = View.VISIBLE

    }

    private fun clearDataBase() {
        viewModel.clearDb()
    }

    private fun hasReadStoragePermission(): Boolean {
        return EasyPermissions.hasPermissions(
            requireContext(),
            android.Manifest.permission.READ_EXTERNAL_STORAGE
        )
    }

    private fun readStorageTask() {
        if (hasReadStoragePermission()) {
            clearDataBase()
            getCategories()
        } else {
            EasyPermissions.requestPermissions(
                this,
                "This app needs access to your storage,",
                READ_STORAGE_PERM,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onRationaleDenied(requestCode: Int) {

    }

    override fun onRationaleAccepted(requestCode: Int) {

    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {

    }

}