package com.herickcarlsen.recipeapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.herickcarlsen.recipeapp.R
import com.herickcarlsen.recipeapp.entities.Ingredient
import kotlinx.android.synthetic.main.item_rv_ingredient.view.*

class IngredientAdapter: RecyclerView.Adapter<IngredientAdapter.RecipeViewHolder>() {

    var ctx: Context? = null
    var arrIngredient = ArrayList<Ingredient>()
    class RecipeViewHolder(view: View): RecyclerView.ViewHolder(view){

    }

    fun setData(arrData : List<Ingredient>){
        arrIngredient = arrData as ArrayList<Ingredient>
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
        ctx = parent.context
        return RecipeViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_rv_ingredient,parent,false))
    }

    override fun getItemCount(): Int {
        return arrIngredient.size
    }

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {

        holder.itemView.tv_ingredient_name.text = arrIngredient[position].ingredient
        holder.itemView.tv_measurement_name.text = arrIngredient[position].measure

    }


}